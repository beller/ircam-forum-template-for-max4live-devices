**Ircam forum Template for Max4Live devices**

This template is dedicated to people who develop M4L devices distributed on the Ircam Forum. It recommends some best practices and gives a few examples.

* **Author**: greg.beller@ircam.fr
* **Release Date**: 01-24-2020
* **Version** 1.0
* **Credits**: This template is largely derived from devices made by Manuel Poletti, Thomas Goepfer and Jean Lochard.
* **Dependencies**: This device uses no depedencies.
* **Compatibility**: Ableton Live 9 or 10, windows or mac.
